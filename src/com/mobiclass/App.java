package com.mobiclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Application;
import android.content.Intent;

import com.mobiclass.entity.Aluno;
import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.AtividadeExecutada;
import com.mobiclass.entity.Curso;
import com.mobiclass.entity.Disciplina;
import com.mobiclass.entity.Turma;
import com.mobiclass.service.Service;

public class App extends Application {

	private Aluno usuario;
	private List<Curso> cursosDoAluno;
	private List<Turma> turmasDoAluno;
	private List<Disciplina> disciplinasDoAluno;
	private List<AtividadeDisponivel> atividadesDisponiveis;
	@Deprecated
	private Map<Turma, Map<Disciplina, List<AtividadeDisponivel>>> entidadesOrganizadas;
	private Map<AtividadeDisponivel, AtividadeExecutada> mapaAtividadeExecutada;
	@Deprecated
	private Map<Disciplina, List<AtividadeDisponivel>> mapaAtividadesPorDisciplina;
	
	public void deslogar(){
		Service.deslogar(usuario.getRA(), usuario.getSenha());
		usuario = null;
		cursosDoAluno = null;
		turmasDoAluno = null;
		disciplinasDoAluno = null;
		startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
	}
	
	@Deprecated
	public void addCurso(Curso curso){
		if(cursosDoAluno == null)
			cursosDoAluno = new ArrayList<Curso>();
		cursosDoAluno.add(curso);
	}
	@Deprecated
	public void addTurma(Turma turma){
		if(turmasDoAluno == null)
			turmasDoAluno = new ArrayList<Turma>();
		turmasDoAluno.add(turma);
	}
	@Deprecated
	public void addDisciplina(Disciplina disciplina){
		if(disciplinasDoAluno == null)
			disciplinasDoAluno = new ArrayList<Disciplina>();
		disciplinasDoAluno.add(disciplina);
	}
	@Deprecated
	public void addAtividadeDisponivel(AtividadeDisponivel atividadeDisponivel){
		if(atividadesDisponiveis == null)
			atividadesDisponiveis = new ArrayList<AtividadeDisponivel>();
		atividadesDisponiveis.add(atividadeDisponivel);
	}
	
	public List<Turma> getTurmasDoAluno() {
		return turmasDoAluno;
	}
	public void setTurmasDoAluno(List<Turma> turmasDoAluno) {
		this.turmasDoAluno = turmasDoAluno;
	}
	public List<Disciplina> getDisciplinasDoAluno() {
		return disciplinasDoAluno;
	}
	public void setDisciplinasDoAluno(List<Disciplina> disciplinasDoAluno) {
		this.disciplinasDoAluno = disciplinasDoAluno;
	}
	public List<Curso> getCursosDoAluno() {
		return cursosDoAluno;
	}
	public void setCursosDoAluno(List<Curso> cursosDoAluno) {
		this.cursosDoAluno = cursosDoAluno;
	}
	public Aluno getUsuario() {
		return usuario;
	}
	public void setUsuario(Aluno usuario) {
		this.usuario = usuario;
	}

	public List<AtividadeDisponivel> getAtividadesDisponiveis() {
		return atividadesDisponiveis;
	}

	public void setAtividadesDisponiveis(List<AtividadeDisponivel> atividadesDisponiveis) {
		this.atividadesDisponiveis = atividadesDisponiveis;
	}

	public Map<Turma, Map<Disciplina, List<AtividadeDisponivel>>> getEntidadesOrganizadas() {
		return entidadesOrganizadas;
	}

	public void setEntidadesOrganizadas(Map<Turma, Map<Disciplina, List<AtividadeDisponivel>>> entidadesOrganizadas) {
		this.entidadesOrganizadas = entidadesOrganizadas;
	}

	public Map<AtividadeDisponivel, AtividadeExecutada> getMapaAtividadeExecutada() {
		if(mapaAtividadeExecutada == null)
			mapaAtividadeExecutada = new HashMap<AtividadeDisponivel, AtividadeExecutada>();
		return mapaAtividadeExecutada;
	}

	public void setMapaAtividadeExecutada(Map<AtividadeDisponivel, AtividadeExecutada> mapaAtividadeExecutada) {
		this.mapaAtividadeExecutada = mapaAtividadeExecutada;
	}

	public Map<Disciplina, List<AtividadeDisponivel>> getMapaAtividadesPorDisciplina() {
		return mapaAtividadesPorDisciplina;
	}

	public void setMapaAtividadesPorDisciplina(
			Map<Disciplina, List<AtividadeDisponivel>> mapaAtividadesPorDisciplina) {
		this.mapaAtividadesPorDisciplina = mapaAtividadesPorDisciplina;
	}
	
	
}

package com.mobiclass;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobiclass.entity.Curso;
import com.mobiclass.entity.Turma;
import com.mobiclass.service.Service;

public class LoginActivity extends Activity {

	private Button btnLogar;
	private EditText raUsuario;
	private EditText senhaUsuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		obterComponentes();
		configurarComponentes();
	}

	private void obterComponentes() {
		btnLogar = (Button) findViewById(R.id.btnLogar);
		raUsuario = (EditText) findViewById(R.id.inputRALogin);
		senhaUsuario = (EditText) findViewById(R.id.inputSenhaLogin);
	}

	private boolean habilitadoParaEfetuarTransacao() {
		boolean habilitado = true;
		String strNomeUsuario = raUsuario.getText().toString();
		String strSenhaUsuario = senhaUsuario.getText().toString();
		if (strNomeUsuario == null || strNomeUsuario.isEmpty()) {
			Toast.makeText(this, "RA invalido", Toast.LENGTH_SHORT).show();
			habilitado = false;
		} else if (strSenhaUsuario == null || strSenhaUsuario.isEmpty()) {
			Toast.makeText(this, "Senha invalida", Toast.LENGTH_SHORT).show();
			habilitado = false;
		}
		return habilitado;
	}

	private void configurarComponentes() {
		btnLogar.setOnClickListener(new ActionLogar());
	}

	private void carregarCursosETurmas(long raUsuario, String senhaUsuario, long raPesquisado) throws Exception {
		try {
			App a = (App) getApplication();
			List<Turma> turmasDoAluno = new ArrayList<Turma>();
			a.setCursosDoAluno(Service.getCursosDoAluno(raUsuario, senhaUsuario, raPesquisado));
			for (Curso c : a.getCursosDoAluno()) {
				for (Turma turma : Service.getTurmasDoAluno(raUsuario, senhaUsuario, c.getCodCurso(), raPesquisado)) {
					turmasDoAluno.add(turma);
				}
			}
			a.setTurmasDoAluno(turmasDoAluno);
		} 
		catch (Exception ex) {
			throw new Exception("Erro ao tentar carregar turmas do usu�rio");
		}
	}

	private void carregarUsuario(String raUsuario, String senhaUsuario, String raPesquisado) throws Exception {
		try {
			App a = (App) getApplication();
			a.setUsuario(Service.getAluno(Long.parseLong(raUsuario),senhaUsuario, Long.parseLong(raPesquisado)));
		} 
		catch (Exception ex) {
			throw new Exception("Erro ao tentar carregar o usu�rio");
		}

	}

	class ActionLogar implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (habilitadoParaEfetuarTransacao()) {
				new TransacaoLogar(v.getContext()).execute();
			}
		}
	}

	class TransacaoLogar extends AsyncTask<Void, Void, Void> {
		private String mensagem;
		private Context c;
		private ProgressDialog dialog = new ProgressDialog(LoginActivity.this);

		public TransacaoLogar(Context c) {
			this.c = c;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading...");
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String ra = raUsuario.getText().toString();
				String senha = senhaUsuario.getText().toString();
				mensagem = Service.logar(ra, senha);
				if (Service.TRUE.equals(mensagem)) {
					carregarUsuario(ra, senha, ra);
					carregarCursosETurmas(Long.valueOf(ra), senha,
							Long.valueOf(ra));
					Intent i = new Intent(c, DisciplinasActivity.class);
					startActivity(i);
					finish();
					mensagem = "Logado";
				}
			} 
			catch (Exception ex) {
				mensagem = "Erro no Login: " + ex.getMessage();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (c != null)
				Toast.makeText(c, mensagem, Toast.LENGTH_SHORT).show();
			dialog.dismiss();
		}
	}
}

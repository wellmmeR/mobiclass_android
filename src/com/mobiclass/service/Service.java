package com.mobiclass.service;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobiclass.entity.Aluno;
import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.AtividadeExecutada;
import com.mobiclass.entity.Curso;
import com.mobiclass.entity.Disciplina;
import com.mobiclass.entity.JogoForca;
import com.mobiclass.entity.Pontuacao;
import com.mobiclass.entity.Turma;
import com.mobiclass.entity.enumerator.MetodosMgrAcademico;
import com.mobiclass.entity.enumerator.MetodosMgrAtividade;


public class Service {

	private static final int WS_ATV = 1;
	private static final int WS_ACDMC = 2;
	
	private static final String IP = "http://192.168.1.146:8080";
	private static final String NAMESPACE_ATV = "http://atividade.mgr.jpa/";
	private static final String NAMESPACE_ACDMC = "http://academico.mgr.jpa/";
	private static final String WSDL_ATV = IP + "/MOBICLASS/atividade?wsdl";
	private static final String WSDL_ACDMC = IP + "/MOBICLASS/academico?wsdl";
	private static final String SOAP_ACTION_ATV = IP+ "/MOBICLASS/atividade";
	private static final String SOAP_ACTION_ACDMC = IP+ "/MOBICLASS/academico";
	
	private static final String SEPARADOR = "-";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	public static String logar(String raUsuario, String senhaUsuario) throws Exception{
		try{
			SoapObject soap = new SoapObject(NAMESPACE_ATV, "loginAluno");
			soap.addProperty("arg0", raUsuario);
			soap.addProperty("arg1", senhaUsuario);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			
			envelope.setOutputSoapObject(soap);
			HttpTransportSE androidTransport = new HttpTransportSE(WSDL_ATV);
			androidTransport.call(SOAP_ACTION_ATV, envelope);
			SoapPrimitive sp = (SoapPrimitive) envelope.getResponse();
			String[] mensagem = sp.toString().split(SEPARADOR);
			
			if(mensagem[0].equals("true"))
				return mensagem[0];
			else
				return mensagem[1];
		}
		catch(Exception ex){
			throw ex;
		}
		
	}
	
	public static void deslogar(long raUsuario, String senhaUsuario){
		try{
			SoapObject soap = new SoapObject(NAMESPACE_ATV, "logoffAluno");
			soap.addProperty("arg0", raUsuario);
			soap.addProperty("arg1", senhaUsuario);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			
			envelope.setOutputSoapObject(soap);
			HttpTransportSE androidTransport = new HttpTransportSE(WSDL_ATV);
			androidTransport.call(SOAP_ACTION_ATV, envelope);
		}
		catch(Exception ex){
			Log.e("Erro ao tentar deslogar", ex.getMessage(), ex);
		}
		
	}
	
	public static Aluno getAluno(long raUsuario, String senhaUsuario, long ra) throws Exception
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raUsuario);
			argumentos.add(senhaUsuario);
			argumentos.add(ra);
			SoapSerializationEnvelope envelope = getResposta(WS_ACDMC, MetodosMgrAcademico.getAluno.getNome(), argumentos);
			
			SoapPrimitive resposta =(SoapPrimitive) envelope.getResponse();
			Aluno aluno = null;
			if (resposta != null) 
			{
				aluno = new GsonBuilder().setDateFormat("dd/MM/yyyy").create().fromJson(resposta.toString(), Aluno.class);
			}
			return aluno;
			
		}
		catch(Exception ex){
			throw new Exception("Erro ao buscar aluno: " + ex.getMessage());
		}		
	}
	
	public static List<Disciplina> getDisciplinasDoALuno(long raUsuario, String senhaUsuario, String codTurma, long raAluno, int status) throws Exception
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raUsuario);
			argumentos.add(senhaUsuario);
			argumentos.add(codTurma);
			argumentos.add(raAluno);
			argumentos.add(status);
			SoapSerializationEnvelope envelope = getResposta(WS_ACDMC, "getDisciplinaAlunoTurma", argumentos);
			KvmSerializable response = (KvmSerializable) envelope.bodyIn;
			List<Disciplina> lista = new ArrayList<Disciplina>();
			if (response != null) 
			{
				for (int i = 0; i < response.getPropertyCount(); i++) 
				{
					lista.add(new Gson().fromJson(((SoapPrimitive) response.getProperty(i)).toString(), Disciplina.class));
				}	
			}
			return lista;
			
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	public static List<Curso> getCursosDoAluno(long raUsuario, String senhaUsuario, long raAluno) throws Exception
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raUsuario);
			argumentos.add(senhaUsuario);
			argumentos.add(raAluno);
			
			SoapSerializationEnvelope envelope = getResposta(WS_ACDMC, MetodosMgrAcademico.getCursoAluno.getNome(), argumentos);
			KvmSerializable response = (KvmSerializable) envelope.bodyIn;
			List<Curso> cursos= new ArrayList<Curso>();
			
			if (response != null) 
			{
				
				for (int i = 0; i < response.getPropertyCount(); i++) 
				{
					Curso c = new Gson().fromJson(((SoapPrimitive)response.getProperty(i)).toString(), Curso.class);
					cursos.add(c);
				}
			}
			return cursos;		
		}
		catch(Exception ex)
		{
			throw new Exception("Erro ao buscar Cursos do aluno: " + ex.getMessage());
		}
	}
	
	public static List<Turma> getTurmasDoAluno(long raUsuario, String senhaUsuario, String codCurso, long raAluno) throws Exception{
		try{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raUsuario);
			argumentos.add(senhaUsuario);
			argumentos.add(codCurso);
			argumentos.add(raAluno);
			SoapSerializationEnvelope envelope = getResposta(WS_ACDMC, MetodosMgrAcademico.getTurmasCursoAluno.getNome(), argumentos);
			
			KvmSerializable response = (KvmSerializable) envelope.bodyIn;
			List<Turma> turmas = new ArrayList<Turma>();
			if (response != null) {
				for (int i = 0; i < response.getPropertyCount(); i++) {
					Turma turma = new Gson().fromJson(((SoapPrimitive) response.getProperty(i)).toString(), Turma.class);
					turmas.add(turma);
				}
			}
			return turmas;
			
		}
		catch(Exception ex){
			throw ex;
		}
	}
	
	private static SoapSerializationEnvelope getResposta(int tipoWS, String nomeMetodo, List<Object> argumentos) throws Exception{
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		
		String namespace = WS_ATV == tipoWS ? NAMESPACE_ATV : NAMESPACE_ACDMC;
		String wsdl = WS_ATV == tipoWS ? WSDL_ATV : WSDL_ACDMC;
		String soapAction = WS_ATV == tipoWS ? SOAP_ACTION_ATV : SOAP_ACTION_ACDMC;
		SoapObject soap = new SoapObject(namespace, nomeMetodo);
		for(int i=0; i < argumentos.size(); i++)
			soap.addProperty("arg"+i, argumentos.get(i));
		
		if(tipoWS == 1 || tipoWS == 2)
		{
			envelope.setOutputSoapObject(soap);
			HttpTransportSE androidTransport = new HttpTransportSE(wsdl);
			androidTransport.call(soapAction, envelope);	
		}
		else{
			throw new Exception("Nenhum WS encontrado");
		}
		return envelope;
	}
	
	public static List<AtividadeDisponivel> getAtividadesDisponiveis(long raPessoa,String senhaPessoa, 
			String codDisciplina, long areaAtividade,String codTurma, int anoTurma) throws Exception
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codDisciplina);
			argumentos.add(areaAtividade);
			argumentos.add(codTurma);
			argumentos.add(anoTurma);
			SoapSerializationEnvelope envelope = getResposta(WS_ATV, MetodosMgrAtividade.getAtividadeDisponivel.getNome(), argumentos);
			KvmSerializable resposta = (KvmSerializable) envelope.bodyIn;
			
			List<AtividadeDisponivel> atividadesDisponiveis = new ArrayList<AtividadeDisponivel>();
			if(resposta != null)
			{
				for(int i=0; i<resposta.getPropertyCount();i++)
				{
					AtividadeDisponivel atividade = new Gson().fromJson(((SoapPrimitive) resposta.getProperty(i)).toString(), AtividadeDisponivel.class);
					atividadesDisponiveis.add(atividade);
				}
			}
			return atividadesDisponiveis;
		}
		catch(Exception ex)
		{
			throw new Exception("Erro ao buscar atividadesDisponiveis: " + ex.getMessage());
		}
		
	}
	
	public static AtividadeExecutada getAtividadeParaOAluno(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno) throws Exception{
		try{
			
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codAti);
			argumentos.add(codDisciplina);
			argumentos.add(areaAtividade);
			argumentos.add(codTurma);
			argumentos.add(anoTurma);
			argumentos.add(raAluno);
			SoapSerializationEnvelope envelope = getResposta(WS_ATV, MetodosMgrAtividade.getAtividadeExecutada.getNome(), argumentos);
			SoapPrimitive resposta =(SoapPrimitive) envelope.getResponse();
			AtividadeExecutada atividadeExecutada = null;
			if(resposta != null)
			{
				atividadeExecutada = new Gson().fromJson(resposta.toString(), AtividadeExecutada.class);
			}
			return atividadeExecutada;
		}
		catch(Exception ex)
		{
			throw new Exception("Erro ao buscar atividades para o Aluno: " + ex.getMessage());
		}
	}
	
	public static List<JogoForca> getJogosForca(long raPessoa, String senhaPessoa, 
			String codDisciplina, String codAtividade, long areaAtividade) throws Exception
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codDisciplina);
			argumentos.add(codAtividade);
			argumentos.add(areaAtividade);
			SoapSerializationEnvelope envelope = getResposta(WS_ATV, MetodosMgrAtividade.getJogosForca.getNome(), argumentos);
			KvmSerializable resposta = (KvmSerializable) envelope.bodyIn;
			List<JogoForca> jogos = new ArrayList<JogoForca>();
			if(resposta != null)
			{
				for(int i=0; i<resposta.getPropertyCount();i++)
				{
					JogoForca jogo = new Gson().fromJson(((SoapPrimitive) resposta.getProperty(i)).toString(), JogoForca.class);
					jogos.add(jogo);
				}
			}
			return jogos;
		}
		catch(Exception ex)
		{
			throw new Exception("Erro ao buscar jogo da forca: " + ex.getMessage());
		}
		
	}

	public static void marcarInicioAtividade(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno)
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codAti);
			argumentos.add(codDisciplina);
			argumentos.add(areaAtividade);
			argumentos.add(codTurma);
			argumentos.add(anoTurma);
			argumentos.add(raAluno);
			getResposta(WS_ATV, MetodosMgrAtividade.marcarInicioAtividadeExecutada.getNome(), argumentos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}

	public static void marcarFimAtividade(long raPessoa,String senhaPessoa, 
			String codAti, String codDisciplina, long areaAtividade,String codTurma, int anoTurma, long raAluno, Double nota)
	{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codAti);
			argumentos.add(codDisciplina);
			argumentos.add(areaAtividade);
			argumentos.add(codTurma);
			argumentos.add(anoTurma);
			argumentos.add(raAluno);
			argumentos.add(nota.toString());
			getResposta(WS_ATV, MetodosMgrAtividade.marcarFimAtividadeExecutada.getNome(), argumentos);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	public static double getPontuacaoAluno(long raPessoa,
			String senhaPessoa, long raAluno, String codDisciplina,
			String codTurma, int anoTurma, String codAtividade,
			long areaAtividade) throws Exception{
		try
		{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(raAluno);
			argumentos.add(codDisciplina);
			argumentos.add(codTurma);
			argumentos.add(anoTurma);
			argumentos.add(codAtividade);
			argumentos.add(areaAtividade);
			SoapSerializationEnvelope envelope = getResposta(WS_ATV, MetodosMgrAtividade.getPontuacaoAluno.getNome(), argumentos);
			KvmSerializable resposta = (KvmSerializable) envelope.bodyIn;
			double nota = 0D;
			if(resposta != null)
			{
				for(int i=0; i<resposta.getPropertyCount();)
				{
					Pontuacao pontuacao = new Gson().fromJson(((SoapPrimitive) resposta.getProperty(i)).toString(), Pontuacao.class);
					nota = pontuacao.getPontos();
					break;
				}
			}
			return nota;
		}
		catch(Exception ex)
		{
			throw new Exception("Erro ao buscar pontuacao do aluno: " + ex.getMessage());
		}
	}
	
	public static List<Curso> todosOsCursos(long raUsuario, String senhaUsuario){
		try{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raUsuario);
			argumentos.add(senhaUsuario);
			argumentos.add(1);
			SoapSerializationEnvelope envelope = getResposta(WS_ACDMC, MetodosMgrAcademico.getCursos.getNome(), argumentos);
			
			KvmSerializable response = null;
			
			response = (KvmSerializable) envelope.bodyIn;
			List<String> lista = new ArrayList<String>();
			List<Aluno> alunos = new ArrayList<Aluno>();
			if (response != null) {
				SoapPrimitive[] soapPrimitive = new SoapPrimitive[response.getPropertyCount()];
				for (int i = 0; i < response.getPropertyCount(); i++) {
					soapPrimitive[i] = (SoapPrimitive) response.getProperty(i);
					lista.add(soapPrimitive[i].toString());
					alunos.add(new GsonBuilder().setDateFormat("dd/MM/yyyy").create().fromJson(soapPrimitive[i].toString(), Aluno.class));
				}
			}
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}		
		return null;
	}
	
	public static String getNomeAtividade(long raPessoa, String senhaPessoa,
			String codAtividade, long areaAtividade, String codDisciplina){
		try{
			List<Object> argumentos = new ArrayList<Object>();
			argumentos.add(raPessoa);
			argumentos.add(senhaPessoa);
			argumentos.add(codAtividade);
			argumentos.add(areaAtividade);
			argumentos.add(codDisciplina);
			
			SoapSerializationEnvelope envelope = getResposta(WS_ATV, MetodosMgrAtividade.getNomeAtividade.getNome(), argumentos);
			SoapPrimitive sp = (SoapPrimitive) envelope.getResponse();
			return sp.toString();
		}
		catch(Exception ex){
			ex.printStackTrace();
			return "ERRO";
		}
		
	}
	
	
}

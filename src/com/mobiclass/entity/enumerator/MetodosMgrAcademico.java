package com.mobiclass.entity.enumerator;

public enum MetodosMgrAcademico {
	addCurso("addCurso"),
	addAluno("addAluno"),
	addProfessor("addProfessor"),
	addTurma("addTurma"),
	addDisciplina("addDisciplina"),
	removeDisciplinaAluno("removeDisciplinaAluno"),
	removeAlunoTurma("removeAlunoTurma"),
	getTurmasProfessorAno("getTurmasProfessorAno"),
	getTurmasProfessor("getTurmasProfessor"),
	getDisciplinaProfessor("getDisciplinaProfessor"),
	getAlunosTurma("getAlunosTurma"),
	getCursos("getCursos"),
	getAlunos("getAlunos"),
	getProfessores("getProfessores"),
	getTurmas("getTurmas"),
	getDisciplinas("getDisciplinas"),
	getDisciplinasTurma("getDisciplinasTurma"),
	getTurmasCursoAluno("getTurmasCursoAluno"),
	getTurmasAluno("getTurmasAluno"),
	getDisciplinaAlunoTurma("getDisciplinaAlunoTurma"),
	getCursoAluno("getCursoAluno"),
	setAlunoCurso("setAlunoCurso"),
	setProfessorTurma("setProfessorTurma"),
	setDisciplinaProfessor("setDisciplinaProfessor"),
	setDisciplinaTurma("setDisciplinaTurma"),
	setAlunoTurma("setAlunoTurma"),
	getAluno("getAluno"),
	loginAluno("loginAluno"),
	loginProfessor("loginProfessor"),
	logoffAluno("logoffAluno"),
	logoffProfessor("logoffProfessor");
	
	private String nome;
	private MetodosMgrAcademico(String nome) {
		this.nome = nome;
	}
	
	public String getNome(){
		return nome;
	}
	
}

package com.mobiclass.entity;

import java.io.Serializable;

public class Atividade implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private AtividadeID idAtividade;
	private long id;
	private String nome;
	private int disponivel;
	
	
	//Construtores;
	public Atividade(){
		setIdAtividade(new AtividadeID());
	}
	public Atividade(String codigoAtividade, String nome, String codigoDisciplina, int isDisponivel, long areaAtividade){
		setIdAtividade(new AtividadeID(codigoAtividade, codigoDisciplina, areaAtividade));
		setNome(nome);
		setDisponivel(isDisponivel);
		
	}
	
	public long getId(){
		return id;
	}
	
	public int isDisponivel() {
		return disponivel;
	}
	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return "||"+idAtividade.getCodigoAtividade()+'|'+idAtividade.getAreaAtividade()+'|'+getNome()+'|'+idAtividade.getCodigoDisciplina()+"||";
	}
	public AtividadeID getIdAtividade() {
		return idAtividade;
	}
	public void setIdAtividade(AtividadeID idAtividade) {
		this.idAtividade = idAtividade;
	}	
}

package com.mobiclass.entity;

import java.io.Serializable;

public class TurmaID implements Serializable{

	private static final long serialVersionUID = -3591303718387106415L;
	
	public TurmaID(){}

	public TurmaID(String codigoTurma, int anoTurma){
		this.codigoTurma = codigoTurma;
		this.anoTurma = anoTurma;
	}
	
	private String codigoTurma;
	private int anoTurma;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anoTurma;
		result = prime * result
				+ ((codigoTurma == null) ? 0 : codigoTurma.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurmaID other = (TurmaID) obj;
		if (anoTurma != other.anoTurma)
			return false;
		if (codigoTurma == null) {
			if (other.codigoTurma != null)
				return false;
		} else if (!codigoTurma.equals(other.codigoTurma))
			return false;
		return true;
	}

	public String getCodigoTurma() {
		return codigoTurma;
	}

	public void setCodigoTurma(String codigoTurma) {
		this.codigoTurma = codigoTurma;
	}

	public int getAnoTurma() {
		return anoTurma;
	}

	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}

}

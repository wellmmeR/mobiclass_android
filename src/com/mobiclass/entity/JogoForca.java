package com.mobiclass.entity;


public class JogoForca {

	private long codJogo;
	private String codAtividade;
	private String codDisciplina;
	private long areaAtividade;
	private int disponivel;
	private String pergunta;
	private String palavra;
	private String dica;
	private int numTentativas;

	
	public JogoForca(){
		
	}
	
	public JogoForca(String codAtividade, String codDisciplina,
			long areaAtividade, int disponivel, String pergunta, String palavra, String dica, int numTentativas) {
		super();
		this.codAtividade = codAtividade;
		this.codDisciplina = codDisciplina;
		this.areaAtividade = areaAtividade;
		this.disponivel = disponivel;
		this.pergunta = pergunta;
		this.palavra = palavra;
		this.dica = dica;
		this.numTentativas = numTentativas;
	}

	public long getCodJogo() {
		return codJogo;
	}
	public void setCodJogo(long codJogo) {
		this.codJogo = codJogo;
	}
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}
	public int getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(int disponivel) {
		this.disponivel = disponivel;
	}
	public String getPalavra() {
		return palavra;
	}
	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}
	public String getDica() {
		return dica;
	}
	public void setDica(String dica) {
		this.dica = dica;
	}
	public int getNumTentativas() {
		return numTentativas;
	}
	public void setNumTentativas(int numTentativas) {
		this.numTentativas = numTentativas;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (areaAtividade ^ (areaAtividade >>> 32));
		result = prime * result
				+ ((codAtividade == null) ? 0 : codAtividade.hashCode());
		result = prime * result
				+ ((codDisciplina == null) ? 0 : codDisciplina.hashCode());
		result = prime * result + (int) (codJogo ^ (codJogo >>> 32));
		result = prime * result + ((dica == null) ? 0 : dica.hashCode());
		result = prime * result + disponivel;
		result = prime * result + numTentativas;
		result = prime * result + ((palavra == null) ? 0 : palavra.hashCode());
		result = prime * result
				+ ((pergunta == null) ? 0 : pergunta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JogoForca other = (JogoForca) obj;
		if (areaAtividade != other.areaAtividade)
			return false;
		if (codAtividade == null) {
			if (other.codAtividade != null)
				return false;
		} else if (!codAtividade.equals(other.codAtividade))
			return false;
		if (codDisciplina == null) {
			if (other.codDisciplina != null)
				return false;
		} else if (!codDisciplina.equals(other.codDisciplina))
			return false;
		if (codJogo != other.codJogo)
			return false;
		if (dica == null) {
			if (other.dica != null)
				return false;
		} else if (!dica.equals(other.dica))
			return false;
		if (disponivel != other.disponivel)
			return false;
		if (numTentativas != other.numTentativas)
			return false;
		if (palavra == null) {
			if (other.palavra != null)
				return false;
		} else if (!palavra.equals(other.palavra))
			return false;
		if (pergunta == null) {
			if (other.pergunta != null)
				return false;
		} else if (!pergunta.equals(other.pergunta))
			return false;
		return true;
	}
}

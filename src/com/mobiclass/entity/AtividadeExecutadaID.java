package com.mobiclass.entity;

import java.io.Serializable;

public class AtividadeExecutadaID implements Serializable{

	private static final long serialVersionUID = -5643687196831461890L;
	
	private Long codAluno;
	private String codAtividade;
	private String codDisciplina;
	private String codTurma;
	private int anoTurma;
	private long areaAtividade;
	
	public AtividadeExecutadaID() {
	
	}
	public AtividadeExecutadaID(long codAluno, String codAtividade, String codDisciplina, String codTurma, int anoTurma, long areaAtividade) {
		this.codAluno = codAluno;
		this.codAtividade = codAtividade;
		this.codDisciplina = codDisciplina;
		this.codTurma = codTurma;
		this.anoTurma = anoTurma;
		this.areaAtividade = areaAtividade;
	}
	
	public Long getCodAluno() {
		return codAluno;
	}
	public void setCodAluno(Long codAluno) {
		this.codAluno = codAluno;
	}
	public String getCodAtividade() {
		return codAtividade;
	}
	public void setCodAtividade(String codAtividade) {
		this.codAtividade = codAtividade;
	}
	public String getCodDisciplina() {
		return codDisciplina;
	}
	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
	public String getCodTurma() {
		return codTurma;
	}
	public void setCodTurma(String codTurma) {
		this.codTurma = codTurma;
	}
	public int getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(int anoTurma) {
		this.anoTurma = anoTurma;
	}
	public long getAreaAtividade() {
		return areaAtividade;
	}
	public void setAreaAtividade(long areaAtividade) {
		this.areaAtividade = areaAtividade;
	}

}

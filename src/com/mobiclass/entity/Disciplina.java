package com.mobiclass.entity;

import java.io.Serializable;

public class Disciplina implements Serializable {
	private static final long serialVersionUID = 1L;

	private String codigoDisciplina;

	private String nome;

	public Disciplina() {
	}


	public Disciplina(String codigoDisciplina, String nome) {
		setCodigoDisciplina(codigoDisciplina);
		setNome(nome);
	}

	public String getCodigoDisciplina() {
		return codigoDisciplina;
	}

	public void setCodigoDisciplina(String codigoDisciplina) {
		this.codigoDisciplina = codigoDisciplina;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String toString() {
		return "||" + getCodigoDisciplina() + '|' + getNome() + "||";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codigoDisciplina == null) ? 0 : codigoDisciplina.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Disciplina other = (Disciplina) obj;
		if (codigoDisciplina == null) {
			if (other.codigoDisciplina != null)
				return false;
		} else if (!codigoDisciplina.equals(other.codigoDisciplina))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
	
}

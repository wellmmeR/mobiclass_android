package com.mobiclass.adapter.item;

import java.io.Serializable;

import com.mobiclass.entity.AtividadeDisponivel;
import com.mobiclass.entity.AtividadeExecutada;

public class AtividadeListItem implements Serializable{
	
	private static final long serialVersionUID = -3828196689296922326L;
	private AtividadeExecutada atividadeExecutada;
	private AtividadeDisponivel atividadeDisponivel;
	private String nome;
	private int tipo;
	private int img;
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getImg() {
		return img;
	}
	public void setImg(int img) {
		this.img = img;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public AtividadeExecutada getAtividadeExecutada() {
		return atividadeExecutada;
	}
	public void setAtividadeExecutada(AtividadeExecutada atividadeExecutada) {
		this.atividadeExecutada = atividadeExecutada;
	}
	public AtividadeDisponivel getAtividadeDisponivel() {
		return atividadeDisponivel;
	}
	public void setAtividadeDisponivel(AtividadeDisponivel atividadeDisponivel) {
		this.atividadeDisponivel = atividadeDisponivel;
	}

}

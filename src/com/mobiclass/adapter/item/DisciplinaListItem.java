package com.mobiclass.adapter.item;

import java.io.Serializable;

import com.mobiclass.entity.Disciplina;

public class DisciplinaListItem implements Serializable{

	private static final long serialVersionUID = -5243027297600386204L;
	
	public  static final String NOME_CURSO = "Curso: ";
	public static final String NOME_TURMA = "Turma: ";
	
	private String nomeTurma;
	private Integer anoTurma;
	private String nomeCurso;
	
	private Disciplina disciplina;
	
	public DisciplinaListItem() {
	}
	
	
	
	public DisciplinaListItem(String nomeTurma, Integer anoTurma,
			String nomeCurso, Disciplina disciplina) {
		super();
		this.nomeTurma = nomeTurma;
		this.anoTurma = anoTurma;
		this.nomeCurso = nomeCurso;
		this.disciplina = disciplina;
	}



	public String getNomeTurma() {
		return nomeTurma;
	}
	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}
	public String getNomeCurso() {
		return nomeCurso;
	}
	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}
	public Integer getAnoTurma() {
		return anoTurma;
	}
	public void setAnoTurma(Integer anoTurma) {
		this.anoTurma = anoTurma;
	}
	public Disciplina getDisciplina() {
		return disciplina;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
}
